package main

import (
	"fmt"
	"runtime"
)

var i int
var c = make(chan int, 1)

func add() {
	c <- 0
	i++
	fmt.Println(i)
	<-c
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	i = 0
	for i := 0; i < 10; i++ {
		go add()
	}
	for i < 10 {}
}
