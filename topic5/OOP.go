package main

import "fmt"

type Operationer interface {
	operate() string
}

type Car struct{}

func (car *Car) operate() string {
	return "drive a car"
}

type Card struct{}

func (card *Card) operate() string {
	return "withdraw 5 million"
}

type Human struct {
	name    string
	operate Operationer
}

func main() {
	human := Human{name: "Zhangsan"}
	car := Car{}
	human.operate = &car
	invoke(human)
	card := Card{}
	human.operate = &card
	invoke(human)
}

func invoke(human Human)  {
	switch v := human.operate.(type) {
	case *Car:
		fmt.Println(human.name, v.operate())
	case *Card:
		fmt.Println(human.name, v.operate())
	}
}
