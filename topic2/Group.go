package main

import (
	"fmt"
	"unicode"
)

func main() {

	fmt.Print("please enter a string:")
	str := ""
	fmt.Scan(&str)
	chineseCount, letterCount, digitCount, otherCount := 0, 0, 0, 0
	for _, w := range str {
		if unicode.Is(unicode.Scripts["Han"], w) {
			chineseCount += 1
		} else if unicode.IsLetter(w) {
			letterCount += 1
		} else if unicode.IsDigit(w) {
			digitCount += 1
		} else {
			otherCount += 1
		}
	}
	fmt.Printf(
		"stats=> \n total: %d \n chinses: %d \n letter: %d \n digit: %d \n other: %d",
		len([]rune(str)), chineseCount, letterCount, digitCount, otherCount,
	)
}
