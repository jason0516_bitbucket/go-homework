package main

import (
	"reflect"
	"fmt"
)

type Person struct {
	Name string
	Age  uint
}
type Student struct {
	Person
	Score uint
}

func (stu *Student) SetScore(score uint) bool {
	stu.Score = score
	return true
}

func main() {
	person := Person{Name: "Zhangsan", Age: 18}
	student := Student{}
	student.Person = person
	r := reflect.ValueOf(&student)
	m := r.MethodByName("SetScore")
	args := []reflect.Value{reflect.ValueOf(uint(100))}
	m.Call(args)
	fmt.Println(student)
}
