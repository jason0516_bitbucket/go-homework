package main

import "fmt"

func main() {
	edge := 7
	for i := 1; i <= edge; i++ {
		for space := 0; space < edge-i; space++ {
			fmt.Print(" ")
		}
		for star := 0; star < 2*i-1; star++ {
			fmt.Print("*")
		}
		fmt.Println()
	}
	for i := edge - 1; i >= 1; i-- {
		for space := 0; space < edge-i; space++ {
			fmt.Print(" ")
		}
		for star := 0; star < 2*i-1; star++ {
			fmt.Print("*")
		}
		fmt.Println()
	}
}
