package main

import (
	"math/rand"
	"time"
	"fmt"
)

type Student struct {
	Name   string
	Gander string
	Age    int
	Score  int
}

func (student *Student) exam() {
	student.Score = rand.Intn(100)
}

func InsertionSort(students []Student) {
	n := len(students)
	if n < 2 {
		return
	}
	for i := 1; i < n; i++ {
		for j := i; j > 0 && students[j].Score < students[j-1].Score; j-- {
			swap(students, j, j-1)
		}
	}
}

func swap(slice []Student, i int, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}

func main() {
	rand.Seed(time.Now().Unix())
	students := map[uint]*Student{
		100: &Student{"Michael", "male", 16, 0},
		101: &Student{"James", "male", 16, 0},
		102: &Student{"Kobe", "male", 16, 0},
		103: &Student{"Jason", "male", 16, 0},
		104: &Student{"Java", "female", 16, 0},
		105: &Student{"Scala", "female", 16, 0},
		106: &Student{"Golang", "female", 16, 0},
		107: &Student{"Python", "female", 16, 0},
	}

	var afterExam []Student

	for _, v := range students {
		v.exam()
		afterExam = append(afterExam, *v)
	}

	InsertionSort(afterExam)

	for v := range afterExam {
		fmt.Println(afterExam[v])
	}

}
