package main

import "fmt"

type I int

func (i *I) Increase() {
	*i *= 2
}

func main() {
	var i I = 100
	i.Increase()
	fmt.Println(i)
	i.Increase()
	fmt.Println(i)
	i.Increase()
	fmt.Println(i)
}
